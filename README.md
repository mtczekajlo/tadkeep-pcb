# TadKeep PCB

This PCB is designed for `TadKeep` app from [here](https://gitlab.com/mtczekajlo/tadkeep).

## EDA

KiCad 6

```bash
sudo dnf install kicad kicad-packages3d kicad-doc
```

## Schematics

Schematics are plotted in CI/CD - get the latest pipeline [here](https://gitlab.com/mtczekajlo/tadkeep-pcb/pipelines/latest).

## CI/CD

```bash
pip install --no-compile kibot
```
